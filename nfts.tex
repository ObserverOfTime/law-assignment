\documentclass[11pt]{beamer}

% Metadata
\title{NFTs και Νομικά Ζητήματα}
\author{Ιωάννης Σομός}
\institute[]{Οικονομικό Πανεπιστήμιο Αθηνών}
\date{\today}

% Packages
\usepackage[english,greek]{babel}
\usepackage[
  style=english]{csquotes}
\usepackage[
  style=numeric,
  backend=biber,
  sorting=anyt,
  maxnames=4,
  minnames=3]{biblatex}
\usepackage[
  cachedir=minted-cache,
  outputdir=build]{minted}
\usepackage{copyrightbox}
\usepackage{tcolorbox}
\usepackage{moresize}
\usepackage{ragged2e}
\usepackage{fontspec}
\usepackage{pifont}
\usepackage{minted}
\usepackage{tikz}
\usepackage{acro}

\usetikzlibrary{shapes.geometric}

% Beamer options
\usetheme{CambridgeUS}

\titlegraphic{\includegraphics[width=0.5\textwidth]{AUEB}}

\setbeamertemplate{headline}{}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{note page}{
  \insertvrule{1em}{note page.bg}
  \insertnote}
\setbeamertemplate{itemize item}{\ding{228}}
\setbeamertemplate{itemize subitem}{\ding{118}}
\setbeamertemplate{section in toc}{
  \tikz[thick]{
    \node[regular polygon, regular polygon sides=6,
          rounded corners=0.6, scale=0.6,
          fill=auebred, color=auebred, draw] {};
  }\ \ \inserttocsection}

\setbeamerfont{frametitle}{family=\rmfamily}
\setbeamerfont{title}{size=\huge, family=\rmfamily}
\setbeamerfont{author}{size=\LARGE, family=\rmfamily}
\setbeamerfont{institute}{size=\Large, family=\rmfamily}
\setbeamerfont{date}{size=\large, family=\rmfamily}
\setbeamerfont{description item}{series=\bfseries}
\setbeamerfont{enumerate item}{size=\large}

\definecolor{auebred}{HTML}{762124}
\setbeamercolor{frametitle}{bg=auebred}
\setbeamercolor{titlelike}{bg=auebred, fg=white}
\setbeamercolor{itemize item}{fg=auebred}
\setbeamercolor{description item}{fg=auebred}
\setbeamercolor{enumerate item}{fg=auebred}
\setbeamercolor{palette tertiary}{bg=auebred}
\setbeamercolor{bibliography entry author}{fg=black}
\setbeamercolor{bibliography entry note}{fg=black}

\setbeamersize{description width=1em}

% Font options
\setmainfont{Source Serif Pro}
\setsansfont[
  RawFeature={-locl,+cv11}
]{Source Sans Pro}
\setmonofont[Scale=0.9]{Source Code Pro}

% Bibliography options
\addbibresource{cite.bib}

\mode<handout>{
  \nocite{korn2022why,hildobby2022nft}}

\DeclareDelimFormat{finalnamedelim}{\addspace\&\space}

\DeclareFieldFormat[report]{title}{%
  \thefield{title}\addspace\thefield{subtitle}\isdot}
\DeclareFieldFormat{eprint:ssrn}{
    \textsc{ssrn}\addcolon\space
    \href{https://www.ssrn.com/abstract=#1}{#1}}

\DeclareCiteCommand{\citeorg}
  {\usebibmacro{prenote}}
  {\bibhyperref{\printlist{organization}}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

% Minted options
\setminted{
    autogobble=true, numbersep=6pt, style=bw,
    fontsize=\ssmall, frame=single, framesep=1ex}

\AtBeginEnvironment{minted}{
  \renewcommand{\fcolorbox}[4][]{\color{red!40!black}{#4}}}

% TCB options
\tcbuselibrary{skins}

\newtcolorbox{bbox}[2][]{
  beamer, fonttitle=\footnotesize\bfseries, fontupper=\scriptsize,
  colbacktitle=auebred, colframe=gray!15!white, title={#2}, #1}

% Other options
\urlstyle{same}
\hypersetup{
    colorlinks=true, linkcolor=auebred,
    citecolor=teal, urlcolor=cyan}

\acsetup{
  make-links=false, use-id-as-short=true}

\graphicspath{{images/}}

\setlength\fboxsep{1pt}
\setlength\bibitemsep{0.75\itemsep}

% Commands
\newcommand{\udot}{\char"0387}
\newcommand{\copyrightimage}[2]{
  \copyrightbox[b]{\colorbox{auebred}{#1}}{
    \hspace*{\fill}\tiny #2\hspace*{2pt}}}
\newcommand{\tweet}[3][]{
  \href{https://twitter.com/#2/status/#3}{#1}}

\renewcommand{\bibfont}{\ssmall}
\renewcommand{\subtitlepunct}{\addcolon\space}

% Acronyms
\DeclareAcronym{NFT}{
  long={Non-Fungible Token},
  alt={Μη Ανταλλάξιμο Διακριτικό},
  alt-plural-form={Μη Ανταλλάξιμα Διακριτικά},
  long-format={\foreignlanguage{english}}}

\begin{document}

\frame{\titlepage}
\note{
  Το θέμα της παρουσίασης είναι τα νομικά ζητήματα των NFT\@.
}

\begin{frame}{Επισκόπηση}
  \NoHyper
  \tableofcontents
  \endNoHyper
  \note{
    Αρχικά, θα μάθουμε τι είναι τα NFT (χωρίς πολλές τεχνικές λεπτομέρειες),
    ποια είναι η ιστορία τους και τι γίνεται με την ιδιοκτησία τους.
    Έπειτα, θα μιλήσουμε για κάποια θέματα ιδιωτικότητας και παραβίασης
    πνευματικών δικαιωμάτων, καθώς και διάφορες οικονομικές απάτες.
    Και θα τελειώσουμε με ορισμένες παρατηρήσεις και συμπεράσματα.
  }
\end{frame}

\section{Τι εστί NFT;}
\begin{frame}{\secname}
  \begin{description}[<+- >]
    \item[\acs{NFT} (\acl{NFT})]\hfill\\
      Ένα μοναδικό ψηφιακό αναγνωριστικό που είναι καταγεγραμμένο σε
      blockchain, δεν μπορεί να είναι αντιγραφεί ή να ανταλλαχθεί και
      χρησιμοποιείται για την πιστοποίηση της γνησιότητας ενός αντικειμένου.
    \item[Blockchain]\hfill\\
      Μία κατανεμημένη βάση δεδομένων που αποθηκεύει δεδομένα σε blocks τα
      οποία συνδέονται μεταξύ τους με χρήση κρυπτογραφικών τεχνικών, ώστε
      να μην είναι εφικτό να μεταβληθούν μεμονωμένα blocks εκ των υστέρων.
    \item[Smart Contract]\hfill\\
      Ένα πρόγραμμα που εκτελεί ορισμένες ενέργειες σε κάποιο
      blockchain όταν πληρούνται συγκεκριμένες προϋποθέσεις.
  \end{description}
  \note<1>{
    NFT, λοιπόν, είναι ένα μοναδικό ψηφιακό αναγνωριστικό, καταγεγραμμένο
    σε blockchain, που δεν μπορεί να αντιγραφεί ή να ανταλλαχθεί και
    χρησιμοποιείται για την πιστοποίηση της γνησιότητας ενός αντικειμένου.
  }
  \note<2>{
    Για όσους δεν έχουν επιλέξει το μάθημα, blockchain είναι ουσιαστικά μία
    κατανεμημένη βάση δεδομένων που αποθηκεύει δεδομένα σε blocks τα οποία
    συνδέονται μεταξύ τους με χρήση κρυπτογραφικών τεχνικών, ώστε να μην
    είναι εφικτό να μεταβληθούν μεμονωμένα blocks εκ των υστέρων.
  }
  \note<3>{
    Επίσης, κάθε NFT έχει ένα smart contract, που στην πραγματικότητα δεν είναι
    ούτε έξυπνο, ούτε συμβόλαιο. Είναι απλά ένα πρόγραμμα σε κάποιο blockchain
    που εκτελεί ορισμένες ενέργειες όταν πληρούνται συγκεκριμένες προϋποθέσεις.
  }
\end{frame}

\section{Ιστορική αναδρομή}
\begin{frame}{\secname}
  \setbeamercovered{transparent=10}
  \begin{tikzpicture}[scale=0.5,every node/.style={outer sep=5pt}]
    \def\events{{
      {"05/14", "``Quantum'': ο πρόδρομος των NFT~\cite{dash2021nfts}"},
      {"10/15", "``Etheria'': το πρώτο NFT στο Ethereum~\cite{hakki2021nft}"},
      {"09/17", "Προτείνεται το ``Non-fungible Token Standard''~\cite{entriken2017eip}"},
      {"03/21", "Το ακριβότερο ως τώρα NFT πωλείται για \$69,3 εκατ.~\cite{reyburn2021jpg}"},
      {"04/21", "``Bored Ape Yacht Club'': η δημοφιλέστερη σειρά NFT"},
      {"04/22", "Ο\ Sina Estavi λαμβάνει μέγιστη προσφορά \$280 σε\\
                 NFT που αγόρασε ένα χρόνο πριν για \$2,9 εκατ.~\cite{handagama2022jack}"}
    }}
    \pgfmathsetmacro{\length}{5}

    \foreach \i in {0, ..., \length} {
        \pgfmathsetmacro{\date}{\events[\i][0]}
        \pgfmathsetmacro{\event}{\events[\i][1]}
        \draw[thick, auebred] (0, -2 * \i - 2) -- (0, -2 * \i);
        \uncover<+- >{
          \draw(0, -2 * \i - 1) node[left] {\date};
          \draw(0, -2 * \i - 1) node[right, align=left] {\event};
        }
        \filldraw[draw=white, fill=auebred, thick] (0, -2 * \i - 1) circle (0.5em);
        \draw[thick, auebred] (-1em, -2 * \i - 1) -- (0, -2 * \i - 1);
    }
\end{tikzpicture}
  \note<1>{
    Τον Μάιο του 2014, δημιουργήθηκε το ``Quantum'' από τον προγραμματιστή
    Anil Dash και τον καλλιτέχνη Kevin McCoy. Αυτή την ιδέα την ονόμασαν
    τότε ``monetized graphics''. Το Quantum βρισκόταν στο blockchain του
    Namecoin, και όχι στο Ethereum όπου βρίσκονται πλέον τα περισσότερα.
  }
  \note<2>{
    Το Ethereum ξεκίνησε τον Ιούλιο του '15 και τον Οκτώβριο δημιουργήθηκε
    το Etheria, το πρώτο ΝFT σε αυτό. Ακόμα, όμως, δεν υπήρχε ο όρος ``NFT''.
  }
  \note<3>{
    Τον Σεπτέμβριο του '17 προτάθηκε το ERC721, το NFT standard, όπου και τυποποιήθηκε
    ο όρος. Τον ίδιο χρόνο δημιουργήθηκαν διάφορα NFT, όπως CryptoPunks και CryptoKitties,
    τα οποία όμως παρέμειναν στην αφάνεια ως τις αρχές του 2021 όταν τα NFT έγιναν της μόδας.
  }
  \note<4>{
    Τον Μάρτιο του '21 πωλήθηκε ως NFT ένα κολάζ ψηφιακών έργων
    του καλλιτέχνη ``Beeple'' για σχεδόν 70 εκατομμύρια δολάρια.
  }
  \note<5>{
    Έναν μήνα μετά δημιουργήθηκε το ``Bored Ape Yacht Club'', η πλέον δημοφιλέστερη συλλογή
    NFT που προώθησαν και διάφοροι διάσημοι χωρίς να αποκαλύψουν ότι πληρώθηκαν για να το
    κάνουν οπότε και υποβλήθηκε αργότερα ομαδική αγωγή εναντίον τους.~\cite{korn2022why}
  }
  \note<6>{
    Μέχρι τις αρχές του '22, η φούσκα των NFT όλο και διαστελλόταν, αλλά
    μετά\ldots\ έσκασε, όπως φαίνεται και από το περιστατικό του Sina Estavi.
    Εκείνος είχε αγοράσει για σχεδόν 3 εκατομμύρια δολάρια ένα πραγματικά
    αξιοθαύμαστο screenshot του πρώτου tweet\ldots\ και αποφάσισε να το
    βγάλει σε δημοπρασία πιστεύοντας ότι θα λάβει προσφορά έως 50
    εκατομμύρια αλλά τελικά έλαβε μόλις 280 δολάρια και δεν το πούλησε.
    Γεγονός που έρχεται σε αντίθεση με τις υποσχέσεις των υπερμάχων
    των NFT ότι θα ανέβουν σίγουρα σε αξία με την πάροδο του χρόνου.
  }
\end{frame}

\section{Ιδιοκτησία των NFT}
\begin{frame}{\secname}
  \pause
  \begin{center}
    \copyrightimage
      {\includegraphics[height=0.8\textheight]{Sacks}}
      {\copyright~\tweet[Adam Sacks]{AdamSacks}{1450162259733491715}}
  \end{center}
  \note{
    Όταν αγοράζεις ένα αντικείμενο, είσαι ο ιδιοκτήτης του. Όταν,
    όμως, αγοράζεις το NFT ενός αντικειμένου, δεν σου ανήκει αυτόματα
    και το ίδιο το αντικείμενο, αλλά μόνο μία απόδειξη ότι το αγόρασες.
  }
\end{frame}

\begin{frame}[fragile]{\secname}{Πρότυπο ERC721}
  \vfill
  \begin{minted}[label={Μεταδεδομένα~\cite{entriken2017eip,opensea10erc}}]{json}
    {
      "name": "Herbie Starbelly",
      "description": "Friendly OpenSea Creature that enjoys long swims in the ocean.",
      "image": "https://storage.googleapis.com/opensea-prod.appspot.com/creature/50.png",
      "attributes": [...]
    }
  \end{minted}
  \vspace*{1em}
  \begin{itemize}[<+(1)- >]
    \item Περιέχει σύνδεσμο προς κάποιο αρχείο~\cite{dash2021nfts}
    \item Δεν παρέχει δικαίωμα ιδιοκτησίας του
          αρχείου~\cite{boehm2022intellectual,moringiello2021property}
    \item Το αρχείο μπορεί να μεταβληθεί ή να διαγραφεί~\cite{chohan2021nonfungible}
  \end{itemize}
  \vfill
  \note<1>{
    Σύμφωνα με το πρότυπο ERC721, τα NFT περιέχουν ορισμένα
    μεταδεδομένα, όπως αυτά του παραδείγματος από τη πλατφόρμα OpenSea,
    μία από τις δημοφιλέστερες πλατφόρμες αγοραπωλησίας NFT\@. Όνομα,
    περιγραφή, εικόνα (γιατί συνήθως εικόνες αντιπροσωπεύουν) και
    οτιδήποτε άλλο θέλει να προσθέσει η πλατφόρμα, όπως ιδιότητες.
  }
  \note<2-3>{
    Βλέπουμε λοιπόν ότι περιέχει έναν σύνδεσμο προς κάποιο αρχείο
    εκτός του blockchain αλλά δεν παρέχει αυτόματα δικαίωμα ιδιοκτησίας
    αυτού του αρχείου ούτε στα μεταδεδομένα αλλά ούτε και στο <<συμβόλαιο>>
    που δεν συμπεριέλαβα γιατί είναι πολύ τεχνικό. Μόνο λίγα project έχουν
    ξεχωριστούς (και αμφίβολα εφαρμόσιμους) όρους χρήσης, επίσης εκτός
    του blockchain, που παραχωρούν κάποια δικαιώματα στον αγοραστή.
  }
  \note<4>{
    Επίσης, τόσο το αρχείο όσο και τα μεταδεδομένα είναι απλά ένα
    link στο internet που μπορεί ανά πάσα στιγμή να πάψει να ισχύει
    κάτι που έχει ήδη συμβεί αρκετές φορές. Υπάρχει η δυνατότητα χρήσης
    του IPFS, που είναι ένα κατανεμημένο σύστημα αρχείων, αλλά ούτε αυτό
    είναι απολύτως αξιόπιστο. Ακόμα, δεν υπάρχει καμία πιστοποίηση ότι
    το αρχείο θα είναι όντως αυτό για το οποίο πλήρωσες αφού όποιος έχει
    πρόσβαση σε αυτό μπορεί να το αλλάξει. Μάλιστα, το παραπάνω παράδειγμα
    το έχω πάρει από το documentation του OpenSea και η εικόνα στο link
    αυτό δεν αντιστοιχεί με την εικόνα που υπάρχει στο ίδιο documentation.
  }
\end{frame}

\section{Προβλήματα ιδιωτικότητας}
\begin{frame}{\secname}
  \textbf{Το blockchain δεν είναι πραγματικά ανώνυμο}~\cite{ravenscraft2022nfts}
  \vspace*{0.5em}
  \begin{itemize}[<+(1)- >]
    \item Οι υπηρεσίες συλλέγουν δεδομένα για τους πελάτες
    \item Οι ενέργειες στο blockchain είναι δημόσια προσβάσιμες
    \item Το πορτοφόλι αποκαλύπτει όλο το ιστορικό συναλλαγών
    \item Δεν απαιτείται έγκριση από τον παραλήπτη ενός NFT
  \end{itemize}
  \note<1-2>{
    Πέρα από θέματα ιδιοκτησίας, τα NFT έχουν και πολλά προβλήματα ιδιωτικότητας.
    Αρχικά, για να αγοράσεις NFT χρειάζεσαι κρυπτονομίσματα, και για να αποκτήσεις
    κρυπτονομίσματα πρέπει να χρησιμοποιήσεις μία υπηρεσία ανταλλαγής κρυπτονομισμάτων.
    Αυτές οι υπηρεσίες συλλέγουν τα ίδια δεδομένα που θα σύλλεγε και μία τράπεζα.
    Δεν υπάρχει η ανωνυμία που ισχυρίζονται η υπέρμαχοι των κρυπτονομισμάτων.
  }
  \note<3>{
    Φυσικά, αυτά τα δεδομένα τα έχει μόνο η υπηρεσία, αλλά όλες οι ενέργειες
    που γίνονται στο blockchain είναι δημόσια προσβάσιμες οπότε μπορεί να
    μην ξέρουν όλοι απαραίτητα ποιος είσαι, αλλά ξέρουν πάντα τι κάνεις.
  }
  \note<4>{
    Όταν αγοράζεις λοιπόν ένα NFT και βάζεις την εικόνα στο προφίλ σου
    στο twitter, γιατί όλοι αυτό κάνουν, μπορεί ο οποιοσδήποτε να βρει
    τη διεύθυνση του πορτοφολιού της συναλλαγής αυτού του NFT και εκεί
    να δει όλες τις άλλες συναλλαγές που έχουν γίνει σε αυτό το πορτοφόλι
    αλλά και να μάθει ότι το τάδε πορτοφόλι αντιστοιχεί στο τάδε άτομο.
  }
  \note<5>{
    Επίσης, όποιος ξέρει τη διεύθυνση μπορεί να σου στείλει οποιοδήποτε
    NFT θέλει χωρίς την έγκρισή σου. Γιατί είναι πρόβλημα αυτό; Γιατί,
    όπως είπαμε, η συναλλαγή στο blockchain δεν μπορεί να διαγραφεί και
    το smart contract του NFT μπορεί να περιέχει αυθαίρετο κώδικα. Οπότε,
    αν κάνεις οποιαδήποτε ενέργεια σε αυτό το NFT, για παράδειγμα να το
    <<κάψεις>> (να το στείλεις δηλαδή σε μία κενή διεύθυνση), πρώτων
    πληρώνεις το κόστος αυτής της ενέργειας και δεύτερων ενεργοποιείται
    ο κώδικας και μπορεί να στείλει κρυπτονομίσματα από το πορτοφόλι σου
    στο πορτοφόλι άλλου, πάλι χωρίς την έγκρισή σου. Αυτό όντως συμβαίνει.
  }
\end{frame}

\section{Παραβίαση πνευματικών δικαιωμάτων}
\begin{frame}{\secname}
  \begin{itemize}[<+- >]
    \justifying
    \item Δεν ελέγχεται αν ένα NFT δημιουργείται με τη συγκατάθεση του
          καλλιτέχνη του έργου \cite{gerard2021nfts,guadamuz2021treachery}
    \item Οι παραβιάσεις επιλύονται υποβάλλοντας αίτηση αφαίρεσης του NFT
          στην πλατφόρμα \cite{boehm2022intellectual,guadamuz2021treachery}
    \item Η πλατφόρμα αφαιρεί το NFT από τον κατάλογό της, αλλά δεν μπορεί
          να το διαγράψει από το blockchain \cite{boehm2022intellectual}
    \item Το ``\href{https://www.deviantartprotect.com/}{DeviantArt Protect}''
          έχει εντοπίσει 630 χιλιάδες πιθανές παραβιάσεις μέσα σε ενάμιση χρόνο
  \end{itemize}
  \note<1>{
    Η παραβίαση πνευματικών δικαιωμάτων είναι ένα αρκετά συχνό φαινόμενο
    στα NFT καθώς μπορεί ο οποιοσδήποτε να δημιουργήσει και να πουλήσει NFT
    για έργα καλλιτεχνών, εν ζωή ή αποθανόντων, χωρίς τη συγκατάθεσή τους.
  }
  \note<2>{
    Οι παραβιάσεις αυτές επιλύονται ζητώντας από την πλατφόρμα αγοραπωλησίας
    να αφαιρέσει το NFT σύμφωνα με το DMCA (που υποθέτω είναι γνωστό σε όλους).
    % Ένας νόμος των Ηνωμένων Πολιτειών που προστατεύει τα
    % πνευματικά δικαιώματα σε ψηφιακά ή ψηφιοποιημένα αγαθά.
  }
  \note<3>{
    Η πλατφόρμα, λοιπόν, αφαιρεί το NFT από τον κατάλογό της, εμποδίζοντας την
    πώλησή του σε αυτή. Αλλά, ακόμα κι αν αφαιρεθεί από όλες τις πλατφόρμες, θα
    παραμείνει στο blockchain και μπορεί ακόμα να πουληθεί, απλά πιο δύσκολα.
    Το ίδιο ισχύει και για το επίσης συχνά φαινόμενο της κλοπής NFT.
  }
  \note<4>{
    Αξίζει να σημειωθεί ότι το ``DeviantArt'' (μία ιστοσελίδα για τη δημοσίευση
    έργων τέχνης) δημιούργησε ειδικό σύστημα ανίχνευσης παραβιάσεων πνευματικών
    δικαιωμάτων των έργων που δημοσιεύονται σε αυτή και εντόπισε, μέσα σε
    λιγότερο από ενάμιση χρόνο, 630 χιλιάδες πιθανές παραβιάσεις. Ποιος ξέρει
    πόσες παραβιάσεις έχουν συμβεί συνολικά σε όλες τις αντίστοιχες ιστοσελίδες;
  }
\end{frame}

\section{Οικονομικές απάτες}
\begin{frame}{\secname}
  \setbeamercovered{transparent=10}
  \begin{itemize}
    \item<2- > Ponzi scheme
    \item<2- > Pyramid scheme
    \item<3- > Wash trading
    \item<4- > Pump and dump
    \item<5- > Money laundering
    \item<6- > Insider trading
    \item<7- > Exit scam (rug pull)
  \end{itemize}
  \note<1>{
    Ας δούμε τώρα μερικές κλασσικές οικονομικές απάτες τις οποίες
    όχι μόνο δεν αποτρέπουν τα NFT, αλλά μάλλον τις διευκολύνουν.
  }
  \note<2>{
    Αρχικά, Ponzi και pyramid schemes είναι δύο αρκετά όμοιες απάτες όπου οι
    προηγούμενοι επενδυτές πληρώνονται με τα χρήματα των επόμενων επενδυτών, ενώ
    ο κομπιναδόρος τα βάζει στην τσέπη και συνήθως εξαφανίζεται. Η διαφορά είναι
    ότι στην πυραμίδα τους επόμενους επενδυτές τους στρατολογούν οι προηγούμενοι.
    Κατά τη γνώμη πολλών, τα κρυπτονομίσματα γενικά αλλά και τα NFT είναι μία απάτη
    τέτοιας μορφής\udot\ και όντως, η λογική με την οποία λειτουργούν είναι παρόμοια.
  }
  \note<3>{
    Έπειτα, wash trading είναι η πώληση και αγορά ενός αγαθού από το ίδιο άτομο
    ώστε να φαίνεται ότι έχει μεγαλύτερη ζήτηση και να αυξήσει την αξία του για
    μελλοντική πώληση. Σύμφωνα με μία ανάλυση που βρήκα, τον Ιανουάριο του '22, το
    wash trading αποτελούσε το 80\% του όγκου συναλλαγών σε NFT\@.~\cite{hildobby2022nft}
  }
  \note<4>{
    Pump and dump είναι μία απάτη στην οποία ένα ή περισσότερα πρόσωπα
    διαδίδουν ψευδείς πληροφορίες για κάποιο επενδυτικό αγαθό ώστε να
    αγοράσουν άλλοι σε μεγαλύτερη τιμή και μετά αυτοί να πουλήσουν και
    να τσεπώσουν τη διαφορά. Το αναφέρω γιατί είναι συχνό φαινόμενο σε
    κρυπτονομίσματα αλλά δεν βρήκα αν υπάρχει συγκεκριμένα στα NFT\@.
  }
  \note<5>{
    Money laundering είναι το <<ξέπλυμα>> χρημάτων που αποκτήθηκαν
    παράνομα. Θα το δούμε πιο αναλυτικά σε λίγο, όπως και τα επόμενα.
  }
  \note<6>{
    Insider trading είναι η αθέμιτη εκμετάλλευση εμπιστευτικών
    πληροφοριών για κάποιο επενδυτικό αγαθό για προσωπικό κέρδος.
  }
  \note<7>{
    Τέλος, exit scam ή αλλιώς rug pull είναι μία, απ' ό,τι φαίνεται, πολύ
    συχνή απάτη στον κόσμο των NFT στην οποία οι απατεώνες εξαφανίζονται με
    τα χρήματα των επενδυτών χωρίς ποτέ να εκπληρώσουν τις υποσχέσεις τους.
  }
\end{frame}

\begin{frame}{\secname}{Money laundering}
  \begin{center}
    \copyrightimage
      {\includegraphics[width=0.9\textwidth]{chainanalysis}}
      {\citeorg[Source:~][]{grauer2022crime}}
  \end{center}
  \note{
    Βλέπουμε, λοιπόν, ότι το 2021 το ξέπλυμα βρώμικου χρήματος με χρήση
    NFT είχε ξεπεράσει συνολικά τα 3 εκατομμύρια δολάρια. Όμως, η ίδια
    έρευνα βρήκε ότι με κρυπτονομίσματα γενικότερα είχαν ξεπλυθεί 8,6
    δισεκατομμύρια δολάρια ενώ ο OHE εκτιμάει το ποσό που ξεπλένεται
    κάθε χρόνο με συμβατικά μέσα (συνήθως πωλήσεις πινάκων) στα 1 με 2
    τρισεκατομμύρια δολάρια. Οπότε το ξέπλυμα με NFT είναι μια σταγόνα
    στον ωκεανό, ίσως γιατί δεν τα εμπιστεύονται ούτε οι ίδιοι οι απατεώνες.
    Πιθανών βέβαια το ποσό να αυξήθηκε αλλά δεν βρήκα πιο πρόσφατη έρευνα.
  }
\end{frame}

\begin{frame}{\secname}{Αξιοσημείωτες υποθέσεις}
  \pause
  \begin{bbox}{U.S. v. Chastain, 22 CRIM 305 (S.D.N.Y.)}
    \begin{quote}
      \lbrack\ldots\rbrack\ NATHANIEL CHASTAIN, the defendant, misappropriated
      OpenSea's confidential business information about what NFTs were going
      to be featured on its homepage and used that information to secretly
      purchase dozens of NFTs shortly before they were featured.
      CHASTAIN then sold the NFTs he had purchased at a profit shortly
      after the NFTs were featured.~\textup{\cite{doj2022former}}
    \end{quote}
  \end{bbox}
  \pause
  \begin{bbox}{U.S. v. Nguyen and Llacuna, 22 mag 2478 (S.D.N.Y.)}
    \begin{quote}
      NGUYEN and LLACUNA used the Internet to advertise and sell
      approximately \$1.1 million in non-fungible tokens (``NFT''),
      which included representations that the purchaser would receive
      certain benefits, including giveaways and access to a metaverse game,
      when in reality there were no such benefits.~\textup{\cite{doj2022defendants}}
    \end{quote}
  \end{bbox}
  \note<1>{
    Έχουν υπάρξει μέχρι τώρα διάφορες νομικές υποθέσεις σχετικές με NFT\@.
    Εδώ θα μιλήσω για δύο συλλήψεις οι οποίες, όταν εκτελεσθούν οι δίκες,
    θα θέσουν νομικό προηγούμενο καθώς είναι οι πρώτες στο είδος τους.
  }
  \note<2>{
    \footnotesize
    Ο Nathaniel Chastain αντιμετωπίζει κατηγορίες για wire fraud
    (συγκεκριμένα insider trading) και για money laundering. Ως
    υπάλληλος της πλατφόρμας OpenSea γνώριζε ποια NFT θα προβληθούν
    στην αρχική σελίδα. Προφανώς, όταν προβάλλονται και τα βλέπει
    όλος ο κόσμος, ανεβαίνει η αξία τους. Οπότε, εκείνος αγόραζε,
    από άλλους λογαριασμούς (εξ ου και η δεύτερη κατηγορία),
    ή τα ίδια ή άλλα NFT των ίδιων δημιουργών πολύ φτηνά και,
    μόλις προβάλλονταν, τα πουλούσε σε πολύ μεγαλύτερη τιμή.
    Κατέθεσε αίτηση να απορριφθούν οι κατηγορίες εναντίον
    υποστηριζόμενος: πρώτον ότι η πληροφορία που είχε στα χέρια
    του δεν είχε κάποια αξία οπότε δεν μπορεί να κατηγορηθεί για
    wire fraud\udot\ δεύτερον ότι απλά μετέφερε χρήματα από άλλους
    λογαριασμούς οπότε δεν μπορεί να κατηγορηθεί ούτε για money
    laundering\udot\ και τρίτον ότι δεν είναι insider trading
    διότι τα NFT δεν θεωρούνται ``securities'' (δηλαδή χρεόγραφα,
    ή μετοχές ουσιαστικά). Ο δικαστής απέρριψε την αίτηση λέγοντας
    ότι τα πρώτα δύο επιχειρήματα δεν μπορούν να κριθούν τώρα αλλά
    θα πρέπει να τα θέσει στη δίκη για να αποφασίσουν οι ένορκοι,
    ενώ το τρίτο επιχείρημα δεν έχει σημασία καθώς η κατηγορία
    για wire fraud είναι ευρύτερη και απλά υπάρχει αναφορά σε
    insider trading, οπότε θα μπορούσε ενδεχομένως να σβηστεί
    αυτή η αναφορά αλλά όχι να απορριφθεί η ίδια η κατηγορία.
  }
  \note<3>{
    Οι Ethan Nguyen και Andre Llacuna κατηγορούνται για wire
    fraud (συγκεκριμένα rug pull) και επίσης για money laundering.
    Είχαν μία σειρά NFT που ονόμαζαν ``Frosties'' και υπόσχονταν
    διάφορα προνόμια στους αγοραστές. Μόλις, όμως, ξεπούλησαν
    τα NFT, αυτοί εξαφανίστηκαν με τα 1 εκατομμύριο δολάρια που
    εισέπραξαν. Χρησιμοποίησαν, μάλιστα, το ``TornadoCash'',
    μία υπηρεσία σχεδιασμένη για ξέπλυμα κρυπτονομισμάτων οπότε
    κατηγορούνται και για αυτό. Επίσης, όταν συνελήφθησαν, ετοίμαζαν
    μάλλον να διαπράξουν ξανά αυτή την απάτη, με μία νέα σειρά NFT.
  }
\end{frame}

\section{Παρατηρήσεις και συμπεράσματα}
\begin{frame}{\secname}
  \begin{itemize}
    \item Δεν λύνουν το πρόβλημα της ψηφιακής ιδιοκτησίας
    \item Δεν είναι πραγματικά ασφαλή και αποκεντρωμένα
    \item Είναι επιρρεπή σε μια πληθώρα από απάτες
    \item Τελικά, δεν αξίζουν τον κόπο και το κόστος
  \end{itemize}
  \note{
    Είδαμε, λοιπόν, ότι τα NFT δεν λύνουν τελικά το πρόβλημα
    της αγοραπωλησίας και ιδιοκτησίας ψηφιακών αγαθών, όπως
    ισχυρίζονται οι υπέρμαχοί τους\udot\ δεν είναι πραγματικά
    ασφαλή, καθώς τα αντικείμενα που αντιπροσωπεύουν μπορούν
    να μεταβληθούν ή να διαγραφούν αλλά και τα ίδια τα NFT
    μπορούν να κλαπούν\udot\ ούτε είναι αποκεντρωμένα, αφού
    η κεντρική πλατφόρμα αποφασίζει αν θα παραμείνουν σε
    αυτή\udot\ είναι, όπως είδαμε, επιρρεπή σε πολλές
    απάτες\udot\ και τελικά δεν αξίζουν ούτε τον κόπο
    ούτε το χρηματικό και περιβαλλοντικό κόστος.
  }
\end{frame}

\begin{frame}[allowframebreaks]{Αναφορές}
  \justifying
  \selectlanguage{english}
  \printbibliography[heading=none]
\end{frame}

\end{document}
