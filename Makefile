BIBER := biber
LATEXMK := latexmk
PYMPRESS := pympress

.DEFAULT_GOAL := build/presentation.pdf

all: build/handout.pdf build/presentation.pdf

build/%.pdf: nfts.tex
	@MODE=$(basename $(@F)) $(LATEXMK) $< \
		$(if $(findstring B,$(firstword -$(MAKEFLAGS))),-g,)

preview: nfts.tex
	@$(LATEXMK) $< -pvc

present: build/presentation.pdf
	@$(PYMPRESS) -t 25 -N bottom $<

validate: cite.bib
	@$(BIBER) --tool --output-dir build -V $<

.PHONY:
clean:
	@$(LATEXMK) $(if $(FORCE),-C,-c)
